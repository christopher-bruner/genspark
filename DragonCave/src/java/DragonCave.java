//Project 1 - Dragon Cave
import java.io.InputStream;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class DragonCave {

    public static Object getUserInput(InputStream in) {

        try {
            Scanner scanner = new Scanner(in);
            return scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.print("Invalid input. Game over!");
            return null;
        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.print("Error getting input. Game over!");
            return null;
        }

    }

    public static String processOption(int option) {

        if (option == 1) {
            return "You approach the cave...\nIt is dark and spooky...\nA large dragon jumps out in front of you! He opens his jaws and...\nGobbles you down in one bite!";
        } else {
            return "You approach the cave...\nA large dragon jumps out in front of you! He appears happy to see you. Happy enough to share his treasure with you!";
        }

    }

    public static void main(String[] args) {

        int option;

        System.out.print("You are in a land full of dragons.\n" +
                "In front of you, you see two caves. In one cave, the dragon is friendly\n" +
                "and will share his treasure with you. The other dragon\n" +
                "is greedy and hungry and will eat you in sight.\n" +
                "Which cave will you go into? (1 or 2)");

       Object input = getUserInput(System.in);
       if (input == null) {
           return;
       }

       option = (int) input;
       System.out.print(processOption(option));

    }

}
