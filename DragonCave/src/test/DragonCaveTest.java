import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

class DragonCaveTest {

    DragonCave p;
    ByteArrayInputStream in;
    InputStream sysBackup = System.in;

    @BeforeEach
    void setUp() {

        in = new ByteArrayInputStream("1".getBytes());

        System.setIn(in);

    }

    @Test
    void getUserInput() {

        assertEquals(1, DragonCave.getUserInput(in));

        in = new ByteArrayInputStream("2".getBytes());
        assertEquals(2, DragonCave.getUserInput(in));

        in = new ByteArrayInputStream("a".getBytes());
        assertNull(DragonCave.getUserInput(in));

    }

    @Test
    void processOption() {

        assertTrue(DragonCave.processOption(1).contains("Gobbles you down in one bite!"));
        assertTrue(DragonCave.processOption(2).contains("Happy enough to share his treasure with you!"));

    }

    @AfterEach
    void tearDown() {

        System.setIn(sysBackup);

    }

}