//Project 2 - Guess the Number
import java.io.InputStream;
import java.util.*;

public class GuessTheNumber {

    public static Object getUsersName(InputStream in) {

        try {

            Scanner scanner = new Scanner(in);

            System.out.println("Hello! What is your name?");
            return scanner.nextLine();

        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.print("Error getting input. Game over!");
            return null;
        }

    }

    public static Object getUserInput(InputStream in) {

        try {

            Scanner scanner = new Scanner(in);

            System.out.print("Take a guess.\n");
            return scanner.nextInt();

        } catch (InputMismatchException e) {
            System.out.print("Invalid input. Game over!");
            return null;
        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.print("Error getting input. Game over!");
            return null;
        }

    }

    public static String processGuess(int number, int guess) {

        if (guess == number) {
            return "w";
        } else if (guess < number) {
            return "l";
        } else {
            return "h";
        }

    }

    public static Object playAgainInput(InputStream in) {

        try {

            Scanner scanner = new Scanner(in);

            System.out.println("Would you like to play again? (y or n)");
            return scanner.next();

        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.print("Error getting input. Game over!");
            return null;
        }

    }

    public static void main(String[] args) {

        Random rand = new Random();
        int min = 1;
        int max = 19;
        String playAgain, name;

        name = (String) getUsersName(System.in);
        if (name == null) {
            return;
        }

        do {

            int num = rand.nextInt(max+min)+min;
            int tries = 1;
            int guess = 0;
            boolean win = false;
            System.out.printf("Well, %s, I am thinking of a number between 1 and 20.\n", name);

            while (tries <= 6) {

                Object input = getUserInput(System.in);
                if (input == null) {
                    return;
                }
                guess = (int) input;

                String result = processGuess(num, guess);

                if (result.equals("w")) {

                    win = true;
                    break;

                } else if (result.equals("l")) {

                    System.out.print("Your guess is too low.\n");
                    tries++;

                } else {

                    System.out.print("Your guess is too high.\n");
                    tries++;

                }

            }

            if (win) {
                System.out.printf("Good job, %s! You guessed my number in %d guesses!\n", name, tries);
            } else {
                System.out.print("You ran out of guesses!\n");
            }

           playAgain = (String) playAgainInput(System.in);

        } while (Objects.equals(playAgain, "y"));

    }

}