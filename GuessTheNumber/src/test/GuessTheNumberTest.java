import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GuessTheNumberTest {

    GuessTheNumber p;
    static ByteArrayInputStream in;
    static InputStream sysBackup = System.in;

    @BeforeAll
    static void setUp() {

        System.setIn(in);

    }

    @Test
    void getUsersName() {

        in = new ByteArrayInputStream("Chris".getBytes());
        assertEquals("Chris", GuessTheNumber.getUsersName(in));

    }

    @Test
    void getUserInput() {

        in = new ByteArrayInputStream("1".getBytes());
        assertEquals(1, GuessTheNumber.getUserInput(in));

    }

    @Test
    void processGuess() {

        assertEquals("w", GuessTheNumber.processGuess(10, 10));
        assertEquals("l", GuessTheNumber.processGuess(10, 5));
        assertEquals("h", GuessTheNumber.processGuess(5, 10));

    }

    @Test
    void playAgainInput() {

        in = new ByteArrayInputStream("y".getBytes());
        assertEquals("y", GuessTheNumber.playAgainInput(in));

        in = new ByteArrayInputStream("n".getBytes());
        assertEquals("n", GuessTheNumber.playAgainInput(in));

    }

    @AfterAll
    static void tearDown() {

        System.setIn(sysBackup);

    }

}