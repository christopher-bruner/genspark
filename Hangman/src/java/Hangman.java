import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

class Hangman {

    private final ArrayList<String> HANGMAN_PICS = new ArrayList<>();

    public static final String[] WORDS = {
            "icebox",
            "zombie",
            "squawk",
            "jumbo",
            "kayak",
            "vodka",
            "flyby",
            "subway",
            "onyx",
            "galaxy",
    };

    private String name;
    public static final Random RANDOM = new Random();
    private String wordToFind;
    public char[] wordFound;
    public static final int MAX_GUESSES = 7;
    public int guesses;
    public int score;

    public String[] hangman = new String[6];

    private final ArrayList<String> lettersGuessed = new ArrayList<>();

    public void setName(String name) {
        this.name = name;
    }

    private String getNewWordToFind() {
        return WORDS[RANDOM.nextInt(WORDS.length-1)];
    }

    private void loadHangmanArt() {

        try (Stream<String> lines = Files.lines(Path.of("Hangman/src/resources/art.txt"))) {

            lines.forEach(line -> {

                if (!Objects.equals(line, "")) {
                    this.HANGMAN_PICS.add(line);
                }

            });

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }

    public void newGame() {

        this.loadHangmanArt();

        String[] hangman = new String[6];

        hangman[0] = "  +-----+";
        hangman[1] = "        |";
        hangman[2] = "        |";
        hangman[3] = "        |";
        hangman[4] = "        |";
        hangman[5] = "        ===";

        this.hangman = hangman;

        score = MAX_GUESSES;
        guesses = 0;
        lettersGuessed.clear();
        wordToFind = getNewWordToFind();

        wordFound = new char[wordToFind.length()];
        Arrays.fill(wordFound, '_');

    }

    public boolean wordFound() {
        return wordToFind.contentEquals(new String(wordFound));
    }

    private boolean letterFound(String s) {
        return wordToFind.contains(s);
    }

    public boolean notGuessed(String s) {
        return !lettersGuessed.contains(s);
    }

    public void enterLetter(String s) {

        if (notGuessed(s)) {

            if (letterFound(s)) {

                char[] arr = wordToFind.toCharArray();
                Stream<Character> stream = IntStream.range(0, arr.length).mapToObj(c -> arr[c]);
                final int[] idx = {0};
                stream.forEach(c -> {
                    if (c.equals(s.charAt(0))) {
                        idx[0] = wordToFind.indexOf(c, idx[0]);
                        wordFound[idx[0]] = c;
                        idx[0]++;
                    }
                });

            } else {
                guesses++;
                score--;
                lettersGuessed.add(s);
            }

        } else {
            System.out.println("You have already guessed that letter. Choose again.");
        }

        this.drawHangman(this.guesses);

    }

    public Object getUserName(InputStream in) {

        try {

            Scanner scanner = new Scanner(in);

            System.out.print("Enter Your Name.\n");
            return scanner.next();

        } catch (InputMismatchException e) {
            System.out.print("Invalid input.");
            return null;
        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.print("Error getting input.");
            return null;
        }

    }

    public Object getUserInput(InputStream in) {

        try {

            Scanner scanner = new Scanner(in);

            System.out.print("Guess a letter.\n");
            return scanner.next();

        } catch (InputMismatchException e) {
            System.out.print("Invalid input.");
            return null;
        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.print("Error getting input.");
            return null;
        }

    }

    public Object playAgainInput(InputStream in) {

        try {

            Scanner scanner = new Scanner(in);

            System.out.println("Do you want to play again? (yes or no)");
            return scanner.next();

        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.print("Error getting input. Game over!");
            return null;
        }

    }

    public boolean isPlayAgain() {

        String playAgain = (String) this.playAgainInput(System.in);

        return Objects.equals(playAgain, "y");

    }

    public void recordScore() {

        try {

            Files.writeString(Paths.get("Hangman/src/resources/scores.txt"), String.format("%s: %d\n", this.name, this.score), StandardOpenOption.CREATE, StandardOpenOption.APPEND);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public LinkedHashMap<String, Integer> sortScores() {

        try (Stream<String> stream = Files.lines(Path.of("Hangman/src/resources/scores.txt"))) {

            HashMap<String, Integer> scores = new HashMap<>();

            stream.forEach(line -> {

                String[] spl = line.split(": ");
                scores.put(spl[0], Integer.parseInt(spl[1]));

            });

            List<Map.Entry<String, Integer>> list = new LinkedList<>(scores.entrySet());
            list.sort(Map.Entry.comparingByValue());

            LinkedHashMap<String, Integer> t = new LinkedHashMap<>();
            for (Map.Entry<String, Integer> a : list) {
                t.put(a.getKey(), a.getValue());
            }

            return t;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    private String printWord() {

        final String[] word = {""};
        Stream<Character> characterStream = new String(this.wordFound).chars().mapToObj(i -> (char)i);
        characterStream.forEach(s -> word[0] = String.format("%s %s", word[0], s));

        return word[0];

    }

    public void drawHangman(int parts) {

        String[] hangman = this.hangman;
        int HEAD = 0;
        int BODY = 3;
        int HIPS = 4;
        int LEGS = 6;

        int idx = parts-1;

        if (idx == HEAD) {
            hangman[1] = String.format("%3s", HANGMAN_PICS.get(idx));
            hangman[1] = String.format("%-8s|",hangman[1]);
        }

        if (idx > HEAD && idx <= BODY) {
            hangman[2] = String.format("%3s", HANGMAN_PICS.get(idx));
            hangman[2] = String.format("%-8s|", hangman[2]);
        }

        if (idx > BODY && idx <= HIPS) {
            hangman[3] = String.format("%2s", HANGMAN_PICS.get(idx));
            hangman[3] = String.format("%-8s|", hangman[3]);
        }

        if (idx > HIPS && idx <= LEGS) {
            hangman[4] = String.format("%1s", HANGMAN_PICS.get(idx));
            hangman[4] = String.format("%-8s|", hangman[4]);
        }

    }

    public String printHangman() {

        final String[] str = {""};
        Arrays.stream(this.hangman).forEach(s -> str[0] = String.format("%s%s\n", str[0], s));

        return str[0];

    }

    public String toString() {

        return String.format("%s\n%s\n%s\n", this.printHangman(), String.format("Missed Letters: %s", this.lettersGuessed), printWord());

    }

    public void printWin() {

        System.out.printf("Yes! The secret word is \"%s\"! You have won!\n", this.wordToFind);

        this.recordScore();
        HashMap<String, Integer> scores = this.sortScores();

        String firstHighScoreName = String.valueOf(scores.keySet().stream().findFirst());
        if (Objects.equals(this.name, firstHighScoreName) || firstHighScoreName == null) {
            System.out.println("You have the new high score!");
        }

    }

    public void printLoss() {
        System.out.printf("You ran out of guesses! The secret word was %s\n\n", this.wordToFind);
    }

}
