public class HangmanMain {

    public static void main(String[] args) {

        Hangman hangman = new Hangman();

        Object input = hangman.getUserName(System.in);
        if (input == null) {
            System.out.println("You have to enter a name!");
            System.exit(0);
        }

        if (input instanceof String) {
            hangman.setName((String) input);
        }

        do {

            boolean win = false;
            hangman.newGame();
            System.out.print("H A N G M A N\n");

            while (hangman.guesses < Hangman.MAX_GUESSES) {

                input = hangman.getUserInput(System.in);
                if (input == null) {
                    continue;
                }

                if (input instanceof String) {
                    hangman.enterLetter((String) input);
                }

                System.out.println(hangman);

                if (hangman.wordFound()) {
                    win = true;
                    break;
                }

            }

            if (win) {
                hangman.printWin();
            } else {
                hangman.printLoss();
            }

        } while (hangman.isPlayAgain());

    }

}
