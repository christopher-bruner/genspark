import org.junit.jupiter.api.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

class HangmanTest {

    static Hangman h;
    static ByteArrayInputStream in;
    static InputStream sysBackup = System.in;

    @BeforeAll
    static void setUp() {

        h = new Hangman();

        System.setIn(in);
        h.newGame();

    }

    @Test
    void notGuessed() {

        assertTrue(h.notGuessed("a"));
        assertTrue(h.notGuessed("c"));

        h.enterLetter("a");
        assertFalse(h.notGuessed("a"));

    }

    @Test
    void getUserInput() {

        in = new ByteArrayInputStream("a".getBytes());
        assertEquals("a", h.getUserInput(in));

        in = new ByteArrayInputStream("c".getBytes());
        assertEquals("c", h.getUserInput(in));

    }

    @Test
    void playAgainInput() {

        in = new ByteArrayInputStream("y".getBytes());
        assertEquals("y", GuessTheNumber.playAgainInput(in));

        in = new ByteArrayInputStream("n".getBytes());
        assertEquals("n", GuessTheNumber.playAgainInput(in));

    }

    @Test
    void drawHangman() {

        h.drawHangman(0);
        assertEquals("  +-----+\n" +
                "        |\n" +
                "        |\n" +
                "        |\n" +
                "        |\n" +
                "        ===\n", h.printHangman());

        h.drawHangman(1);
        assertEquals("  +-----+\n" +
                "  O     |\n" +
                "        |\n" +
                "        |\n" +
                "        |\n" +
                "        ===\n", h.printHangman());

       h.drawHangman(2);
        assertEquals("  +-----+\n" +
                "  O     |\n" +
                "  |     |\n" +
                "        |\n" +
                "        |\n" +
                "        ===\n", h.printHangman());

        h.drawHangman(3);
        assertEquals("  +-----+\n" +
                "  O     |\n" +
                "/ |     |\n" +
                "        |\n" +
                "        |\n" +
                "        ===\n", h.printHangman());

        h.drawHangman(4);
        assertEquals("  +-----+\n" +
                "  O     |\n" +
                "/ | \\   |\n" +
                "        |\n" +
                "        |\n" +
                "        ===\n", h.printHangman());

        h.drawHangman(5);
        assertEquals("  +-----+\n" +
                "  O     |\n" +
                "/ | \\   |\n" +
                "  |     |\n" +
                "        |\n" +
                "        ===\n", h.printHangman());

        h.drawHangman(6);
        assertEquals("  +-----+\n" +
                "  O     |\n" +
                "/ | \\   |\n" +
                "  |     |\n" +
                " /      |\n" +
                "        ===\n", h.printHangman());

        h.drawHangman(7);
        assertEquals("  +-----+\n" +
                "  O     |\n" +
                "/ | \\   |\n" +
                "  |     |\n" +
                " / \\    |\n" +
                "        ===\n", h.printHangman());

    }

    @AfterAll
    static void tearDown() {

        System.setIn(sysBackup);

    }

}