package com.humansvsgoblins;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Application extends javafx.application.Application {

    public final int SIZE = 5;
    public final int NUM_GOBLINS = 3;

    public static void gameLoop(Game game, Scene scene) {

//        do {

            scene.setOnKeyPressed((KeyEvent event) -> {

                boolean moved = false;
                do {

                    KeyCode keyEntered = event.getCode();

                    if (keyEntered.isArrowKey()) {
                        moved = game.movePlayer(keyEntered);
                    }

                } while (!moved);

            });

            game.moveGoblins();

//        } while (game.isPlayerAlive() && game.isGoblinsAlive());

    }

    @Override
    public void start(Stage stage) {

        Game game = new Game(SIZE, NUM_GOBLINS);
        game.setAlignment(Pos.CENTER);
        game.setPrefSize(500.0, 500.0);

        Scene scene = new Scene(game);

        stage.setTitle("Humans vs Goblins");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();

        gameLoop(game, scene);

    }

    public static void main(String[] args) {
        launch();
    }

}