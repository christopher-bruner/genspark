package com.humansvsgoblins;

import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class Game extends GridPane {

    private final Random rand = new Random();
    private Human player;
    private ArrayList<Goblin> goblins;
    private Land[][] grid;

    public enum Direction {
        NORTH,
        SOUTH,
        EAST,
        WEST
    }

    public Game(int size, int numGoblins) {

        this.generateLand(size, size);
        this.placeHumanoids(numGoblins, size, size);

        this.setPrefSize(500, 500);

    }

    public boolean isPlayerAlive() {
        return this.player != null;
    }

    public boolean isGoblinsAlive() {
        return this.goblins.size() > 0;
    }

    private void generateLand(int width, int height) {

        this.grid = new Land[width][height];

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {

                Land l = new Land(rand.nextInt(5));

                this.grid[i][j] = l;
//                this.add(l, i+1, j);

            }
        }

    }

    private void placeHumanoids(int totalGoblins, int width, int height) {

        int w, h;
        this.goblins = new ArrayList<>(totalGoblins);

        for (int i = 0; i < totalGoblins; i++) {

            int strength = this.rand.nextInt(10)+1;

            String[] keys = Goblin.ColorCodes.keySet().toArray(new String[0]);
            String color = keys[this.rand.nextInt(keys.length)];

            Goblin goblin = new Goblin(strength, color);
            boolean done;

            do {

                w = rand.nextInt(width);
                h = rand.nextInt(height);

                done = this.grid[w][h].placeOccupant(goblin);

            } while (!done);

            goblin.setCoordinates(new int[]{w, h});

            this.goblins.add(goblin);

        }

        Human human = new Human(10);
        boolean done;

        do {

            w = rand.nextInt(width);
            h = rand.nextInt(height);

            done = this.grid[w][h].placeOccupant(human);

        } while (!done);

        human.setCoordinates(new int[]{w, h});

        this.player = human;

    }

    public boolean movePlayer(String input) {

        Game.Direction d = null;
        switch (input) {
            case "N":
                d = Game.Direction.NORTH;
                break;

            case "S":
                d = Game.Direction.SOUTH;
                break;

            case "E":
                d = Game.Direction.EAST;
                break;

            case "W":
                d = Game.Direction.WEST;
                break;

            default:
                break;
        }

        if (d == null) {
            return false;
        }

        return this.move(this.player, d);

    }

    public boolean movePlayer(KeyCode key) {

        Game.Direction d = null;

        if (key == KeyCode.UP) {
            d = Direction.NORTH;
        } else if (key == KeyCode.DOWN) {
            d = Direction.SOUTH;
        } else if (key == KeyCode.RIGHT) {
            d = Direction.EAST;
        } else if (key == KeyCode.LEFT) {
            d = Direction.WEST;
        }

        if (d == null) {
            return false;
        }

        return this.move(this.player, d);

    }

    public void moveGoblins() {

        List<Direction> values = List.of(Direction.values());

        this.goblins.forEach(goblin -> {

            boolean moved = false;
            while (!moved) {
                Direction d = values.get(this.rand.nextInt(values.size()));
                moved = this.move(goblin, d);
            }

        });

    }

    public boolean move(Humanoid h, Direction d) {

        int[] newCoords = h.getCoordinates();
        int[] oldCoords = h.getCoordinates();

        switch (d) {

            case NORTH:
                newCoords[0]--;
                break;

            case SOUTH:
                newCoords[0]++;
                break;

            case WEST:
                newCoords[1]--;
                break;

            case EAST:
                newCoords[1]++;
                break;

            default:
                return false;

        }

        if (outOfBounds(newCoords)) {
            return false;
        }

        Land oldSpot = this.grid[oldCoords[0]][oldCoords[1]];
        Land newSpot = this.grid[newCoords[0]][newCoords[1]];

        if (newSpot.addOccupant(h)) {

            oldSpot.removeOccupant(h);
            h.setCoordinates(newCoords);

            ArrayList<Treasure> newSpotTreasure = newSpot.getTreasure();
            ArrayList<Treasure> toRemove = new ArrayList<>();

            if (h instanceof Human && !newSpotTreasure.isEmpty()) {

                System.out.println("Adding treasure...");
                for (Treasure t : newSpotTreasure) {

                    h.addToInventory(t);
                    toRemove.add(t);

                }

                newSpot.removeAllTreasure(toRemove);

            }

//            this.checkForCollisions(newSpot);
            return true;

        }

        return false;

    }

    public boolean outOfBounds(int[] coords) {

        for (int i : coords) {
            if (i < 0 || i >= this.grid.length) {
                return true;
            }
        }

        return false;

    }

    public void checkForCollisions(Land spot) {

        ArrayList<Humanoid> occupants = spot.getOccupants();

        if (occupants.size() == 2) {

            Humanoid battleLoser = this.battle(occupants.get(0), occupants.get(1));

            if (battleLoser != null) {

                int[] coords = battleLoser.getCoordinates();
                this.grid[coords[0]][coords[1]].removeOccupant(battleLoser);
                this.kill(battleLoser);

            }

        }

    }

    public Humanoid battle(Humanoid attacker, Humanoid defender) {

        try {

            System.out.println("Battling...");
            do {

                attacker.attack(defender, this.rand.nextInt(attacker.getStrength()));
                TimeUnit.SECONDS.sleep((long) 1.5);

                if (defender.getHealth() <= 0) {
                    break;
                }

                defender.attack(attacker, this.rand.nextInt(defender.getStrength()));
                TimeUnit.SECONDS.sleep((long) 1.5);

            } while (attacker.getHealth() > 0 && defender.getHealth() > 0);

            if (attacker.getHealth() <= 0) {
                return attacker;
            } else if (defender.getHealth() <= 0) {
                return defender;
            }

        } catch (InterruptedException e) {
            System.out.println("Error during battle!");
        }

        return null;

    }

    public void kill(Humanoid h) {

        if (h instanceof Human) {
            System.out.println("You have died!");
            this.player = null;
        } else if (h instanceof Goblin) {
            System.out.printf("A %s%s%s goblin has died.\n", ((Goblin) h).colorCode, ((Goblin) h).colorName, "\u001b[0m");
            this.goblins.remove(h);
        }

    }

    @Override
    public String toString() {

        String str = "";

        for (Land[] strings : this.grid) {

            for (Land string : strings) {
                str = String.format("%s%s", str, string.toString());
            }

            str = String.format("%s\n", str);

        }

        return str;

    }

}
