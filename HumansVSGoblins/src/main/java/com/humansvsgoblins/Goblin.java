package com.humansvsgoblins;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import java.util.Map;

public class Goblin extends Humanoid {

    public static final Map<String, String> ColorCodes = Map.of(
            "red", "\u001B[31m",
//            "green", "\u001B[32m",
            "yellow", "\u001b[33m",
            "cyan", "\u001B[36m",
            "purple", "\u001B[35m"
    );

    public static final Map<String, Color> Colors = Map.of(
            "red", Color.RED,
            "yellow", Color.YELLOW,
            "cyan", Color.CYAN,
            "purple", Color.PURPLE
    );

    Color color;
    String colorName;
    String colorCode;

    public Goblin(int strength, String color) {

        super();
        this.humanoidType = HumanoidType.GOBLIN;
        this.setStrength(strength);
        this.setHealth(25);

        this.colorName = color;
        this.colorCode = ColorCodes.get(color);
        this.color = Colors.get(color);

        this.setPrefSize(50, 50);
        Circle c = new Circle(15);
        c.setFill(this.color);

        this.getChildren().add(c);

    }

    @Override
    public String toString() {
        String RESET_COLOR = "\u001B[0m";
        return String.format("%s%s%s", this.colorCode, "☹", RESET_COLOR);
    }

}

