package com.humansvsgoblins;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Human extends Humanoid {

    public Human(int strength) {

        super();
        this.humanoidType = HumanoidType.HUMAN;
        this.setStrength(strength);
        this.setHealth(100);

        this.setPrefSize(50, 50);

        Circle c = new Circle(15);
        c.setFill(Color.WHITE);

        this.getChildren().add(c);

    }

    @Override
    public String toString() {
        return "⛇";
    }

}
