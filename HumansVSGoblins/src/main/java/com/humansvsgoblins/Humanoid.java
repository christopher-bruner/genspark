package com.humansvsgoblins;

import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;

import java.util.ArrayList;
import java.util.UUID;



public class Humanoid extends StackPane {

    private final String id;
    private int x, y, health, strength;
    private final ArrayList<Treasure> inventory = new ArrayList<>();
    public HumanoidType humanoidType;

    public enum HumanoidType {
        HUMAN,
        GOBLIN
    };

    public Humanoid() {

        this.id = String.valueOf(UUID.randomUUID());

    }

    public String getUID() {
        return this.id;
    }

    public int[] getCoordinates() {
        return new int[]{this.x, this.y};
    }

    public void setCoordinates(int[] coords) {

        this.x = coords[0];
        this.y = coords[1];

    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getStrength() {
        return strength;
    }

    public int getHealth() {
        return this.health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public ArrayList<Treasure> getInventory() {
        return this.inventory;
    }

    public void addToInventory(Treasure t) {
        System.out.printf("Adding %s to inventory\n", t.name());
        this.inventory.add(t);
    }

    public boolean removeFromInventory(Treasure t) {
        return this.inventory.remove(t);
    }

    public void attack(Humanoid h, int damage) {

        if (h instanceof Human) {
            System.out.print("Goblin attacks human ");
        } else {
            System.out.print("Human attacks goblin ");
        }

        System.out.printf("for %d damage\n", damage);

        h.setHealth(h.getHealth() - damage);

    }

}
