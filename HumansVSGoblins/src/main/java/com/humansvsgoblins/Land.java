package com.humansvsgoblins;

import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.*;
import java.util.stream.Collectors;

public class Land extends StackPane {

    private final ArrayList<Treasure> treasureList = new ArrayList<>(List.of(
            new Treasure("test1", 1000, 1),
            new Treasure("test2", 300, 6),
            new Treasure("test3", 500, 3)
    ));
    private final ArrayList<Humanoid> occupants = new ArrayList<>(2);
    private final ArrayList<Treasure> treasure;

    public Land(int totalTreasure) {

        this.treasure = this.placeTreasure(totalTreasure);

        setPrefSize(100, 100);

        Rectangle r = new Rectangle(90, 90);
        r.setFill(Color.GREEN);

        getChildren().add(r);

    }

    private ArrayList<Treasure> placeTreasure(int totalTreasure) {

        if (totalTreasure == 0) {
            return new ArrayList<>();
        }

        Random rand = new Random();

        int total = rand.nextInt(totalTreasure);
        ArrayList<Treasure> treasures = new ArrayList<>(totalTreasure);

        for (int i = 0; i <= total; i++) {

            int chance = rand.nextInt(10);
            List<Treasure> filteredTreasure = treasureList.stream().filter(c -> c.chance() <= chance).collect(Collectors.toList());

            if (filteredTreasure.isEmpty()) {
                continue;
            }

            int idx = rand.nextInt(filteredTreasure.size());
            Treasure thisTreasure = filteredTreasure.get(idx);
            treasures.add(thisTreasure);

        }

        return treasures;

    }

    public void removeAllTreasure(Collection<Treasure> c) {
        this.treasure.removeAll(c);
    }

    public ArrayList<Treasure> getTreasure() {
        return this.treasure;
    }

    public ArrayList<Humanoid> getOccupants() {
        return this.occupants;
    }

    private Humanoid findOccupant(Humanoid h) {

        for (Humanoid occupant : this.occupants) {

            if (Objects.equals(occupant.getUID(), h.getUID())) {
                return occupant;
            }

        }

        return null;

    }

    public boolean placeOccupant(Humanoid h) {

        if (this.occupants.size() == 1) {
            return false;
        }

        this.getChildren().add(h);
        return this.occupants.add(h);

    }

    public boolean addOccupant(Humanoid h) {

        if (this.occupants.size() == 2) {
            return false;
        }

        if (this.occupants.size() > 0 && this.occupants.get(0) instanceof Goblin && h instanceof Goblin) {
            return false;
        }

        this.getChildren().add(h);
        return this.occupants.add(h);

    }

    public void removeOccupant(Humanoid h) {

        Humanoid i = findOccupant(h);

        if (i != null) {
            this.getChildren().remove(h);
            this.occupants.remove(i);
        }

    }

    @Override
    public String toString() {
        if (this.occupants.size() > 0) {
            return this.occupants.get(0).toString();
        } else {
            return "·";
        }
    }

}
