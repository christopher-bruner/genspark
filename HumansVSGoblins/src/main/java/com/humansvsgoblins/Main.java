package com.humansvsgoblins;

import java.io.InputStream;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

    public static String getUserInput(InputStream in) {

        try {

            Scanner scanner = new Scanner(in);
            System.out.print("Pick a direction to move (N/S/E/W).\n");
            return scanner.next();

        } catch (InputMismatchException e) {
            System.out.print("Invalid input.");
            return null;
        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.print("Error getting input.");
            return null;
        }

    }

    public static String getPlayAgainInput(InputStream in) {

        try {
            Scanner scanner = new Scanner(in);
            System.out.println("Play again? y/n");
            return scanner.next();
        } catch (InputMismatchException e) {
            System.out.print("Invalid input.");
            return null;
        } catch (NoSuchElementException | IllegalStateException e) {
            System.out.print("Error getting input.");
            return null;
        }

    }

    public static void gameLoop(Game game) {

        do {

            System.out.println(game);

            String input;
            boolean moved = false;
            do {

                input = getUserInput(System.in);
                if (input == null) {
                    continue;
                }

                moved = game.movePlayer(input);

            } while (!moved);

            game.moveGoblins();

        } while (game.isPlayerAlive() && game.isGoblinsAlive());

    }

//    public static void main(String[] args) {

//        Game game;

//        String playAgain = "y";
//
//        do {
//
//            game = new Game(5, 5, 5);
//            gameLoop(game);
//            playAgain = getPlayAgainInput(System.in);
//
//        } while (Objects.equals(playAgain, "y"));

//    }

}
