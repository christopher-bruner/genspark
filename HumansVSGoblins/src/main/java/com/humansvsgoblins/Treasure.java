package com.humansvsgoblins;

import java.util.Objects;

public final class Treasure {
    private final String name;
    private final int value;
    private final int chance;

    public Treasure(String name, int value, int chance) {
        this.name = name;
        this.value = value;
        this.chance = chance;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
//        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Treasure) obj;
        return Objects.equals(this.name, that.name) &&
                this.value == that.value &&
                this.chance == that.chance;
    }

    @Override
    public String toString() {
        return "Treasure[" +
                "name=" + name + ", " +
                "value=" + value + ", " +
                "chance=" + chance + ']';
    }

    public String name() {
        return name;
    }

    public int value() {
        return value;
    }

    public int chance() {
        return chance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value, chance);
    }


}
