module com.example.humansvsgoblins {
    requires javafx.controls;
    requires javafx.fxml;

    requires com.almasb.fxgl.all;

    opens com.humansvsgoblins to javafx.fxml;
    exports com.humansvsgoblins;
}