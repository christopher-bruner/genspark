package com.genspark.SpringBootDemo.Controller;

import com.genspark.SpringBootDemo.Entity.Customer;
import com.genspark.SpringBootDemo.Service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/customers")
    public List<Customer> getCustomers() {
        return this.customerService.getAllCustomers();
    }

    @GetMapping("/customers/{id}")
    public Customer getCustomer(@PathVariable String id) {
        return this.customerService.getCustomerById(Integer.parseInt(id));
    }

    @PostMapping("/customers")
    public Customer addCustomer(@RequestBody Customer customer) {
        return this.customerService.addCustomer(customer);
    }

    @PutMapping("/customers/{id}")
    public Customer updateCustomer(@RequestBody Customer customer) {
        return this.customerService.updateCustomer(customer);
    }

    @DeleteMapping("/customers/{id}")
    public boolean deleteCustomer(@PathVariable String id) {
        return this.customerService.deleteCustomer(Long.parseLong(id));
    }

}
