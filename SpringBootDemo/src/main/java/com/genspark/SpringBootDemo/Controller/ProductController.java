package com.genspark.SpringBootDemo.Controller;

import com.genspark.SpringBootDemo.Entity.Product;
import com.genspark.SpringBootDemo.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public String home() {
        return "Welcome!";
    }

    @GetMapping("/products")
    public List<Product> getProducts() {
        return this.productService.getAllProducts();
    }

    @GetMapping("/products/{id}")
    public Product getProduct(@PathVariable String id) {
        return this.productService.getProductById(Integer.parseInt(id));
    }

    @PostMapping("/products")
    public Product addProduct(@RequestBody Product product) {
        return this.productService.addProduct(product);
    }

    @PutMapping("/products")
    public Product updateProduct(@RequestBody Product product) {
        return this.productService.updateProduct(product);
    }

    @DeleteMapping("/products/{id}")
    public boolean deleteProduct(@PathVariable String id) {
        return this.productService.deleteProduct(Long.parseLong(id));
    }

}
