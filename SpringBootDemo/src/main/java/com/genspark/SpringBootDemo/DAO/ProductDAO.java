package com.genspark.SpringBootDemo.DAO;

import com.genspark.SpringBootDemo.Entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDAO extends JpaRepository<Product, Long> {
}
