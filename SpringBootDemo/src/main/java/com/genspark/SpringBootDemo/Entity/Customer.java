package com.genspark.SpringBootDemo.Entity;

import javax.persistence.*;

public @Entity @Table(name = "customers") class Customer {

    private @Id @GeneratedValue(strategy = GenerationType.AUTO) @Column(name = "id") long id;
    private @Column(name = "name") String Name;
    private @Column(name = "address") String Address;
    private @Column(name = "phone") String PhoneNumber;

    public Customer() {}

    public Customer(long id, String name, String address, String phoneNumber) {
        this.id = id;
        Name = name;
        Address = address;
        PhoneNumber = phoneNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

}
