package com.genspark.SpringBootDemo.Entity;

import javax.persistence.*;

public @Entity @Table(name = "products") class Product {

    private @Id @GeneratedValue(strategy = GenerationType.AUTO) @Column(name = "id") long id;
    private @Column(name = "color") String Color;
    private @Column(name = "name") String Name;
    private @Column(name = "description") String Description;

    public Product() {
    }

    public Product(long id, String color, String name, String description) {
        this.id = id;
        Color = color;
        Name = name;
        Description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

}
