package com.genspark.SpringBootDemo.Service;

import com.genspark.SpringBootDemo.Entity.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomers();
    Customer getCustomerById(long id);
    Customer addCustomer(Customer customer);
    Customer updateCustomer(Customer customer);
    boolean deleteCustomer(long id);

}
