package com.genspark.SpringBootDemo.Service;

import com.genspark.SpringBootDemo.DAO.CustomerDAO;
import com.genspark.SpringBootDemo.Entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerDAO customerDAO;

//    List<Customer> list;
//
//    public CustomerServiceImpl() {
//
//        list = new ArrayList<>();
//
//        list.add(new Customer(1, "Peter Griffin", "31 Spooner Street", "5551234567"));
//        list.add(new Customer(2, "Glenn Quagmire", "29 Spooner Street", "5557707727"));
//
//    }

    @SuppressWarnings("unused")
    @Override
    public List<Customer> getAllCustomers() {
        return customerDAO.findAll();
//        return list;
    }

    @Override
    public Customer getCustomerById(long id) {

        return customerDAO.findById(id).orElse(null);
//        for (Customer customer : list) {
//
//            if (customer.getId() == id) {
//                return customer;
//            }
//
//        }
//
//        return null;

    }

    @Override
    public Customer addCustomer(Customer customer) {
        return customerDAO.save(customer);
//        this.list.add(customer);
//        return customer;
    }

    @Override
    public Customer updateCustomer(Customer customer) {

        customerDAO.saveAndFlush(customer);
        return customer;
//        String name = customer.getName();
//        String address = customer.getAddress();
//        String phone = customer.getPhoneNumber();
//
//        Customer c = this.getCustomerById(customer.getId());
//        if (c != null) {
//            c.setName(name);
//            c.setAddress(address);
//            c.setPhoneNumber(phone);
//        }
//
//        return c;

    }

    @Override
    public boolean deleteCustomer(long id) {

        Customer c = this.getCustomerById(id);
        if (c != null) {
            customerDAO.delete(c);
            return true;
        }

        return false;
//        Customer c = this.getCustomerById(id);
//        if (c != null) {
//            this.list.remove(c);
//            return "deleted";
//        }
//
//        return "not found";

    }

}
