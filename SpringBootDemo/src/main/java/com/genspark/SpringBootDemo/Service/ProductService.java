package com.genspark.SpringBootDemo.Service;

import com.genspark.SpringBootDemo.Entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();
    Product getProductById(long id);
    Product addProduct(Product product);
    Product updateProduct(Product product);
    boolean deleteProduct(long id);

}
