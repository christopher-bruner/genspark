package com.genspark.SpringBootDemo.Service;

import com.genspark.SpringBootDemo.DAO.ProductDAO;
import com.genspark.SpringBootDemo.Entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductDAO productDAO;

//    List<Product> list;
//
//    public ProductServiceImpl() {
//
//        list = new ArrayList<>();
//
//        list.add(new Product(1, "black", "towel", "a black towel"));
//        list.add(new Product(2, "gray", "towel", "a gray towel"));
//
//    }

    @SuppressWarnings("unused")
    @Override
    public List<Product> getAllProducts() {
        return productDAO.findAll();
//        return list;
    }

    @Override
    public Product getProductById(long id) {

        return productDAO.findById(id).orElse(null);
//        for (Product product : list) {
//
//            if (product.getId() == id) {
//                return product;
//            }
//
//        }
//
//        return null;

    }

    @Override
    public Product addProduct(Product product) {
        return productDAO.save(product);
//        this.list.add(product);
//        return product;
    }

    @Override
    public Product updateProduct(Product product) {

        productDAO.saveAndFlush(product);
        return product;
//        String name = product.getName();
//        String color = product.getColor();
//        String description = product.getDescription();
//
//        Product p = this.getProductById(product.getId());
//        if (p != null) {
//            p.setName(name);
//            p.setColor(color);
//            p.setDescription(description);
//        }
//
//        return p;

    }

    @Override
    public boolean deleteProduct(long id) {

        Product p = this.getProductById(id);
        if (p != null) {
            productDAO.delete(p);
            return true;
        }

        return false;
//        Product p = this.getProductById(id);
//        if (p != null) {
//            this.list.remove(p);
//            return true;
//        }
//
//        return false;

    }

}
