package org.genspark;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App
{
    public static void main( String[] args )
    {

//        AbstractApplicationContext context = new ClassPathXmlApplicationContext("Spring.xml");
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

//        Student s = (Student) context.getBean("thisStudent");
        Person s = context.getBean(Student.class);

        System.out.println(s);

    }
}
